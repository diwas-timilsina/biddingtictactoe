//  
//   Biding TicTacToe
//   Author: Diwas Timilsina
//   June 2015

///////////////////////////////////////////////////////////////
//                                                           //
//                    CONSTANT STATE                         //

// TODO: DECLARE and INTIALIZE your constants here
var START_TIME = currentTime();
var GRID_SPACING        = 600;
var GRID_LINE_THICKNESS = 4;

var NUMBER_COLOR        = makeColor(0,0,0);
var LINE_COLOR          = makeColor(0.7,0.7,0.7);
var POINT_COLOR         = makeColor(1, 0, 0);
var BOARD_SIDE    = 5;

///////////////////////////////////////////////////////////////
//                                                           //
//                     MUTABLE STATE                         //

// TODO: DECLARE your variables here
var lastKeyCode;
var x;
var y;

///////////////////////////////////////////////////////////////
//                                                           //
//                      EVENT RULES                          //

// When setup happens...
function onSetup() {
    // TODO: INITIALIZE your variables here
    lastKeyCode = 0;
}


// When a key is pushed
function onKeyStart(key) {
    lastKeyCode = key;
}


// Called 30 times or more per second
function onTick() {

    // Draw a white background
    fillRectangle(0, 0, screenWidth, screenHeight, makeColor(1,1,1,1));

    // size of the tic tac toe board
    var pixelX = 620
    var pixelY = 320
    var size = 620
    
    // One square
    var S = size / BOARD_SIDE;
    var x, y, label, player;

    var linecolor = makeColor(0.5, 0.5, 0.5);
    var labelcolor = makeColor(0, 0, 0);
    var thickness = max(1, floor(S / 20));

    // Draw the lines
    for (x = 1; x < BOARD_SIDE; ++x) {
        strokeLine(pixelX + x * S, pixelY, pixelX + x * S, pixelY + size, linecolor, thickness);
        strokeLine(pixelX, pixelY + x * S, pixelX + size, pixelY + x * S, linecolor, thickness);
    }

    

    // clearRectangle(0, 0, screenWidth, screenHeight);

    // fillText("hello world",
    //          screenWidth / 2, 
    //          screenHeight / 2,             
    //          makeColor(0.5, 0.0, 1.0, 1.0), 
    //          "300px Times New Roman", 
    //          "center", 
    //          "middle");

    // fillText(round(currentTime() - START_TIME) + " seconds since start",
    //          screenWidth / 2, 
    //          screenHeight / 2 + 300,   
    //          makeColor(1.0, 1.0, 1.0, 1.0), 
    //          "100px Arial", 
    //          "center", 
    //          "middle");

    // fillText("last key code: " + lastKeyCode,
    //          screenWidth / 2, 
    //          screenHeight / 2 + 500,             
    //          makeColor(0.7, 0.7, 0.7, 1.0), 
    //          "100px Arial", 
    //          "center", 
    //          "middle");
}


///////////////////////////////////////////////////////////////
//                                                           //
//                      HELPER RULES                         //


"""
  Basic Implementation of tic tac toe 
  Author: Diwas Timilsina
  June 2015
"""
import sys
import os
import random 
import getpass

class board:
    """
    Class to maintain game board
    """
 
    def __init__(self):
        self.table = [["-","-","-"],["-","-","-"],["-","-","-"]]
        
    def getPlayer(self,r,c):
        return self.table[r][c]

    def placePlayer(self,r,c,player):
        if self.isEmpty(r,c):
            self.table[r][c] = player.alias
            player.setLocation(r,c)
            self.printBoard()
            return True
        self.printBoard()    
        return False
        
    def isEmpty(self, r,c):
        return True if self.table[r][c] == "-" else False

    def printBoard(self):
        #print ("\n")
        print ("Current state of the board: \n")
        print ("o-------o")
        for r in range(len(self.table)):
            print ("| {0} {1} {2} |".format(self.table[r][0],self.table[r][1],self.table[r][2]))
        print ("o-------o")
        print ("\n")

    def isGameOver(self, r, c):
        # check if a player won
        playerName = self.table[r][c]
        for i in range(1,len(self.table)):
            if playerName != self.table[(r+i)%3][c] and playerName != self.table[r][(c+i)%3] and playerName != self.table[(r+i)%3][(c+i)%3]:
                return False
        return True
            

class player:
    """
      Player Class that stores the attributes of the player
    """

    def __init__(self, name, alias , row = None, column = None):
        self.name = name
        self.row = row
        self.column = column
        self.coins = 4
        self.alias = alias
        self.hasTieBreaker = False
        
    def getName(self):
        return self.name

    def getAlias(self):
        return self.alias

    def deductCoins(self,value):
        self.coins-=value

    def addCoins(self,value):
        self.coins + value

    def __eq__(self, anotherPlayer):
        return True if self.name == anotherPlayer.name else False

    def setLocation(self,r,c):
        self.row = r
        self.column = c

class game():
    """
    Class to generate new game 
    """
    def __init__(self,player1,player2):
        print("#-----------------New Game Statred--------------------------#")
        self.gameBoard = board()
        self.gameBoard.printBoard()
        self.isGameOver = False
 
        self.PLAYER_ONE = player(player1,"X",4)
        self.PLAYER_TWO = player(player2,"O",4)
       
        if random.randint(0,1) == 0:
            self.currentPlayer = self.PLAYER_ONE
            self.PLAYER_ONE.hasTieBreaker = True
        else: 
            self.currentPlayer = self.PLAYER_TWO
            self.PLAYER_ONE.hasTieBreaker = True

        print("{0} has the tie-breaker advantage".format(self.currentPlayer.name))
        #print("{0} choses who goes First!".format(self.currentPlayer.name))
        #self.tieBreakerDecider()
        self.startPlaying()
        
    def tieBreakerDecider(self):
        
        if self.PLAYER_ONE.hasTieBreaker: 
            val = raw_input("{0}, Do you want to keep the tie-breaker advantage? (y/n): ".format(self.PLAYER_ONE.name)).lower()  
            if val == "n" or val == "no":
                self.currentPlayer = self.PLAYER_ONE
                self.PLAYER_ONE.hasTieBreaker = False
                self.PLAYER_TWO.hasTieBreaker = True
            else:
                self.currentPlayer = self.PLAYER_TWO
                self.PLAYER_TWO.hasTieBreaker = False
                self.PLAYER_ONE.hasTieBreaker = True
    
        elif self.PLAYER_TWO.hasTieBreaker:
            val = raw_input("{0}, Do you want to keep the tie-breaker advantage? (y/n): ".format(self.PLAYER_TWO.name)).lower()  
            if val == "n" or val == "no":
                self.currentPlayer = self.PLAYER_TWO
                self.PLAYER_TWO.hasTieBreaker = False
                self.PLAYER_ONE.hasTieBreaker = True
            else:
                self.currentPlayer = self.PLAYER_ONE
                self.PLAYER_ONE.hasTieBreaker = False
                self.PLAYER_TWO.hasTieBreaker = True
    
        
    def conductBiding(self):
        print ("-------------------- Biding----------------------------")
        print ("Ok, {0} time to bid!".format(self.PLAYER_ONE.name))
        bid_player1 = getpass.getpass("{0}, you have {1} coins. Enter your bid value: ".format(self.PLAYER_ONE.name, self.PLAYER_ONE.coins))
        
        while (int(bid_player1) > self.PLAYER_ONE.coins):
            bid_player1 = getpass.getpass("{0}, you have {1} coins. Enter your bid value: ".format(self.PLAYER_ONE.name, self.PLAYER_ONE.coins))
        
        print ("\nOk, {0} time to bid!".format(self.PLAYER_TWO.name))
        bid_player2 = getpass.getpass("{0}, you have {1} coins. Enter your bid value: ".format(self.PLAYER_TWO.name, self.PLAYER_TWO.coins))
       
        while (int(bid_player2) > self.PLAYER_TWO.coins):
            bid_player2 = getpass.getpass("{0}, you have {1} coins. Enter your bid value: ".format(self.PLAYER_TWO.name, self.PLAYER_TWO.coins))
            
        
        if bid_player1 > bid_player2:
            self.PLAYER_ONE.deductCoins(int(bid_player1))
            self.currentPlayer = self.PLAYER_ONE
            self.PLAYER_TWO.addCoins(int(bid_player1))
        elif bid_player1 < bid_player2:
            self.PLAYER_TWO.deductCoins(int(bid_player2))
            self.currentPlayer = self.PLAYER_TWO
            self.PLAYER_ONE.addCoins(int(bid_player2))
        else:
            self.tieBreakerDecider()
            

    def startPlaying(self):
        while True:
            print ("++++++++++++++++++++ New Move ++++++++++++++++++++++++++++++")
            print ("{0} has {1} coins\n{2} has {3} coins".format(self.PLAYER_ONE.name, self.PLAYER_ONE.coins, self.PLAYER_TWO.name, self.PLAYER_TWO.coins))
            
            self.conductBiding()
            print("{0} won the bid! Now make a move!".format(self.currentPlayer.name))
            print ("Rule: The valid rows = [1,2,3] and the valid colums = [1,2,3]")
            #print ("{} Enter where you want to place your mark: ".format(self.currentPlayer.name))
            x, y = raw_input("{0}, choose a board location as 'row col' :".format(self.currentPlayer.name)).split()
            x = int(x)
            y = int(y)
            
            while(x < 1 or x > 3 or y < 1 or y >3):
                x,y = raw_input("Not a vaild move! Go again: ").split()
                x = int(x)
                y = int(y)
              
                
            while not self.gameBoard.placePlayer(x-1,y-1,self.currentPlayer):
                x,y = raw_input("The place is already occupied!, input another location: ").split()
                x = int(x)
                y = int(y)
              
            if self.gameBoard.isGameOver(x-1,y-1):
                print("Game Over!")
                break
            
            if self.currentPlayer == self.PLAYER_ONE:
                self.currentPlayer = self.PLAYER_TWO
            else: 
                self.currentPlayer = self.PLAYER_ONE
        
        print "winner is :"+ self.currentPlayer.name
        
        
if __name__ == "__main__":
    gameBoard = game("Diwas","Sandesh")
    
    


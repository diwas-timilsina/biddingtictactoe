"""
# this file contains alpha beta puring algorithm 
# Author: Diwas Timilsina
# Date: June 2015

"""

import math

def maxValue(state,alpha,beta):
    if state.isTerminal():
        return state.getValueOfState()
    
    val = -float("inf")
    
    for (m,s) in state.nextMoves():
        val = max(val,minValue(s,alpha,beta))
        if val >= beta:
            return val
        alpha = max(alpha,val)
    return val 


def minValue(state,alpha,beta):
    if state.isTermina():
        return state.getValueOfState()
    
    val = float("inf")
    
    for (m,s) in state.nextMoves():
        val = min(val,maxValue(s,alpha,beta))
        if val <= alpha:
            return val
        beta = min(beta,val)
    return val


def alphaBeta(state):
    # search algorithm to determine the best course of action
    # uses alpha-beta pruning of the game tree
    player = state.player
    successors = state.nextMoves()
    
    maxVal = -float("inf")
    bestMove = None
    for (m,s) in successors:
        val = minValue(s, -float("inf"), float("inf"))
        if val > maxVal:
            bestMove = m
    
    return bestMove


"""
Simple graph class
Author : Diwas Timilsina

"""

class Graph():

    def __init__(self):
        #initialize the graph
        self.current_graph = {}
        self.count = 0

    def add(self,vertex):
        # if the vertex is not already in the graph,
        # then add the vertex in the graph dictionary
        if vertex not in self.current_graph:
            self.current_graph[vertex] = []

    def add_edge(self,edge):
        # add an edge between two vertices
        # edge could be a list or a tuple or a set
        edge =  set(edge)
        (node1,node2) = tuple(edge)

        if node1 in self.current_graph:
            self.current_graph[node1].append(node2)
        else:
            self.current_graph[node1] = [node2]
            
    def size(self):
        #size of a graph
        return len(self.vertices())
            
    def edges(self):
        # get all the edges in the graph
        edges = []
        for  vertex in self.current_graph:
            for neighbour in self.current_graph[vertex]:
                if {neighbour,vertex} not in edge:
                    edges.append({vertex,neighbour})
        return edges
        
    def vertices(self):
        # get all the vertices of a graph
        return list(self.current_graph.keys())

    def __str__(self):
        # similar to toString function in java
        str = "Vertices: "
        for vertex in self.current_graph:
            str += repr(vertex) + " "
        str += "\nEdges:"    
        for edges in self.edges():
            str += repr(edge)+ " "
        return str
            
    def next_vert(self):
        # next element
        vertices_coll = self.vertices() 
        result = vertices_coll[self.count]
        
        if not vertices_coll[self.count] == None:
            self.count += 1
            
        return result
            
    def hasNext(self):
                
        # check to see of the there are any vertices left to iterate
        return self.count < self.size()

    def reset_iter(self):
        # reset the counter 
        self.count = 0

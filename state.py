#
# Author: Diwas Timilsina
# Date: June 2015
#

from player import *
from board import *

class State():
    def __init__(self, player,moves,value,board):
        """
        each state stores the following info  
        - name of the player to move
        - stored value at that state 
        - list of moves of the form (x,y) loc on the board
        - board represented a class that represents the board
        """
        
        self.player = player_to_move
        self.moves = moves
        self.value = value
        self.board = board

    def getValueOfState(self):
        return self.value

    def validMoves(self):
        return self.moves

    def computeValue(self,board,move,player):
        x,y = move
        if (board.isGameOver(x,y)):
            if player.alias == "X":
                return 1
            else:
                return -1
        else:
            return 0
            
    def isTerminal(self):
        """
        a state is a terminal state if it the game is won or 
        if there are no empty spaces left on the board
        """
        return (not self.utility == 0) or len(self.moves) == 0

        
    def makeMove(self,move):
        if move not in self.moves:
            return self 
        
        board = self.board.copy()
       
        r,c = move
        board.placePlayer(r,c,self.player)
        
        moves = list(self.moves)
        moves.remove(move)
        
        value = self.computeValue(board,move,self.player)
        playerName = "O" if self.player == "X" else "X"

        # TODO:...
        # change here for the biding code
        newState = State(playerName,moves,value,board)
        
        return newState
        
    def nextMoves(self):
        # return a list of valid (move, state) paris
        return [(move, self.makeMove(move)) for move in self.validMoves]
    

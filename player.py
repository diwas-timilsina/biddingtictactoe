# 
# Author: Diwas Timilsina
# Date: June 2015  
    
class player:
    """
      Player Class that stores the attributes of the player
    """

    def __init__(self, name, alias , row = None, column = None):
        self.name = name
        self.row = row
        self.column = column
        self.coins = 4
        self.alias = alias
        self.hasTieBreaker = False
        
    def getName(self):
        return self.name

    def getAlias(self):
        return self.alias

    def deductCoins(self,value):
        self.coins-=value

    def addCoins(self,value):
        self.coins + value

    def __eq__(self, anotherPlayer):
        return True if self.name == anotherPlayer.name else False

    def setLocation(self,r,c):
        self.row = r
        self.column = c


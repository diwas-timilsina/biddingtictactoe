#
# Author: Diwas Timilsina
# Date: June 2015
#

from player import *

class board:
    """
    class to maintain game board
    """

    def __init__(self):
        self.table = [["-","-","-"],["-","-","-"],["-","-","-"]]
        
    def getPlayer(self,r,c):
        return self.table[r][c]

    def placePlayer(self,r,c,player):
        if self.isEmpty(r,c):
            self.table[r][c] = player.alias
            player.setLocation(r,c)
            self.printBoard()
            return True
        self.printBoard()    
        return False
        
    def isEmpty(self, r,c):
        return True if self.table[r][c] == "-" else False

    def printBoard(self):
        #print ("\n")
        print ("Current state of the board: \n")
        print ("o-------o")
        for r in range(len(self.table)):
            print ("| {0} {1} {2} |".format(self.table[r][0],self.table[r][1],self.table[r][2]))
        print ("o-------o")
        print ("\n")

    def isGameOver(self, r, c):
        # check if a player won
        playerName = self.table[r][c]
        for i in range(1,len(self.table)):
            if playerName != self.table[(r+i)%3][c] and playerName != self.table[r][(c+i)%3] and playerName != self.table[(r+i)%3][(c+i)%3]:
                return False
        return True
